<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180228095925 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE searches ADD status_id INT NOT NULL DEFAULT 1');
        $this->addSql('ALTER TABLE searches ADD CONSTRAINT FK_601838196BF700BD FOREIGN KEY (status_id) REFERENCES statuses (id)');
        $this->addSql('CREATE INDEX IDX_601838196BF700BD ON searches (status_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE searches DROP FOREIGN KEY FK_601838196BF700BD');
        $this->addSql('DROP INDEX IDX_601838196BF700BD ON searches');
        $this->addSql('ALTER TABLE searches DROP status_id');
    }
}
