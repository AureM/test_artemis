<?php

namespace FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Other
 *
 * @ORM\Table(name="others")
 * @ORM\Entity(repositoryClass="FrontBundle\Repository\OtherRepository")
 */
class Other
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Search", mappedBy="other")
     */
    private $searches;

    public function __construct(){
        $this->searches = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Other
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add search
     *
     * @param \FrontBundle\Entity\Search $search
     *
     * @return Other
     */
    public function addSearch(\FrontBundle\Entity\Search $search)
    {
        $this->searches[] = $search;

        return $this;
    }

    /**
     * Remove search
     *
     * @param \FrontBundle\Entity\Search $search
     */
    public function removeSearch(\FrontBundle\Entity\Search $search)
    {
        $this->searches->removeElement($search);
    }

    /**
     * Get searches
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSearches()
    {
        return $this->searches;
    }
}
