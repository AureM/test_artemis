<?php

namespace FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Status
 *
 * @ORM\Table(name="statuses")
 * @ORM\Entity(repositoryClass="FrontBundle\Repository\StatusRepository")
 */
class Status
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=20, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=10)
     */
    private $color;

    /**
     * @ORM\OneToMany(targetEntity="Search", mappedBy="status")
     */
    private $searches;


    public function __construct()
    {
        $this->searches = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Status
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return Status
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Add search
     *
     * @param \FrontBundle\Entity\Search $search
     *
     * @return Status
     */
    public function addSearch(\FrontBundle\Entity\Search $search)
    {
        $this->searches[] = $search;

        return $this;
    }

    /**
     * Remove search
     *
     * @param \FrontBundle\Entity\Search $search
     */
    public function removeSearch(\FrontBundle\Entity\Search $search)
    {
        $this->searches->removeElement($search);
    }

    /**
     * Get searches
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSearches()
    {
        return $this->searches;
    }
}
