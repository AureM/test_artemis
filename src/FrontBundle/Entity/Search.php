<?php

namespace FrontBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FrontBundle\Entity\Status;

/**
 * Search
 *
 * @ORM\Table(name="searches")
 * @ORM\Entity(repositoryClass="FrontBundle\Repository\SearchRepository")
 */
class Search
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Status", inversedBy="searches")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id", nullable=false, columnDefinition="INT NOT NULL DEFAULT 1")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="Other", inversedBy="others")
     * @ORM\JoinColumn(name="other_id", referencedColumnName="id", nullable=false, columnDefinition="INT NOT NULL DEFAULT 1")
     */
    private $other;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=255)
     */
    private $text;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Search
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Search
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set status
     *
     * @param Status $status
     *
     * @return Search
     */
    public function setStatus(Status $status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set other
     *
     * @param \FrontBundle\Entity\Other $other
     *
     * @return Search
     */
    public function setOther(\FrontBundle\Entity\Other $other)
    {
        $this->other = $other;

        return $this;
    }

    /**
     * Get other
     *
     * @return \FrontBundle\Entity\Other
     */
    public function getOther()
    {
        return $this->other;
    }
}
