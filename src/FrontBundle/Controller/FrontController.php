<?php

namespace FrontBundle\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FrontBundle\Entity\Search;
use FrontBundle\Entity\Status;
use FrontBundle\Event\AbstractEvent;
use FrontBundle\FrontBundle;
use FrontBundle\FrontEvents;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class FrontController extends Controller
{
    /**
     * @Route("/", name="front.accueil")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        //Add the search item in DB
        if($request->request->has('search_value')){
            $search = new Search();
            $search->setCreatedAt(new \DateTime());
            $search->setText($request->request->get('search_value'));
            //Set by default to pending status (==1)
            $status = $em->getRepository('FrontBundle:Status')->first();
            $search->setStatus($status);
            $other = $em->getRepository('FrontBundle:Other')->first();
            $search->setOther($other);

            $em->persist($search);
            $em->flush();
        }

        $searches = $em->getRepository('FrontBundle:Search')->findAll();

        //Add listener à la volée
        $dispatcher = $this->get("dispatcher");
        $dispatcher->addListener(
            'front.after_abstract_event',
            function ($event) {
                print_r($event->getAlert());
            }
        );

        //Fire my abstract event here
        $event = new AbstractEvent();
        $dispatcher->dispatch(
            FrontEvents::AFTER_ABSTRACT_EVENT, $event
        );

        return $this->render('FrontBundle:pages:index.html.twig', array('searches'=> $searches));
    }

    /**
     * @Route("/cart", name="front.cart")
     */
    public function cartAction(){
        return $this->render('FrontBundle:pages:cart.html.twig');
    }

    /**
     * @Route("/about", name="front.about")
     */
    public function aboutAction(){
        return $this->render('FrontBundle:pages:about.html.twig');
    }

    /**
     * @Route("/why", name="front.why")
     */
    public function whyAction(){
        return $this->render('FrontBundle:pages:why.html.twig');
    }

    /**
     * @Route("/how", name="front.how")
     */
    public function howAction(){
        return $this->render('FrontBundle:pages:how.html.twig');
    }
}
