<?php

namespace FrontBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class AbstractEvent extends Event
{
    /** @var string */
    protected $alert = 'The event has been fired !!';

    public function getAlert()
    {
        return $this->alert . uniqid();
    }
}