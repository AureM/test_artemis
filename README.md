TEST for ARTEMIS PARTNER
========================

This project is based on Symfony 2 framework (v2.8.32)
You PHP version has to be >= 5.5.9

(Use Fix https://github.com/FriendsOfSymfony/FOSUserBundle/pull/2742/commits/00c366ecdc2dfd3807d7a49424e66a872fc73850)

Within this project I try to demonstrate :

* MVC architecture knowledge 

* Use of PSR standard 1, 2 and 4 (replace 0:deprecated)

* Knowledge of database best practices (SBE, Simple But Enough I think. I can show you on demand more complex but real DB on projects I did)

* Ability to create simple but effective user interfaces : I choose to reproduce your user interface (I need an answer) and to slightly modify it (Modify source of svg picto, add SignIn, SignUp, modify action of main search form). Unfortunately, the TypeKit is not authorized for the domain where I put the code (http://5.196.76.241). So, you will need to add this domain to the list of published domain in TypeKit to access all features (it was working in local probably due to the presence of "localhost" in this list).

* Show us DRY, extensible development : difficult to show that on such small example, but I try to demonstrate it in the view (templating) context.


I deliberately skipped the test part of the demonstration due to lack of time.

Install it ?
------------
* git clone https://AureM@bitbucket.org/AureM/test_artemis.git

* cd test_artemis

* composer install

* Apply fix https://github.com/FriendsOfSymfony/FOSUserBundle/pull/2742/commits/00c366ecdc2dfd3807d7a49424e66a872fc73850

* php app/console doctrine:migration:migrate


What's inside?
--------------
You can access the demo site from **http://bit.ly/2CMAoy4**
You can access the db with **http://bit.ly/2FEzYg7** (username:artemis, pwd:partner)

From your i-need-an-answer template, I add

* Login/Register/Logout system

* A listing of all searches done in the Home page. Each search has a status, and the default, when added is "Pending".

So, first add users with "Sign Up" link, and add as many questions as you want in the Database.
